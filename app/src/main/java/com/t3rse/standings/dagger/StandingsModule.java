package com.t3rse.standings.dagger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.t3rse.standings.configuration.StandingsConfig;
import com.t3rse.standings.service.StandingsApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by DavidSE on 7/8/16.
 */
@Module
public class StandingsModule {

    @Provides
    @Singleton
    OkHttpClient okHttpClient(){
        return new OkHttpClient();
    }

    @Provides
    @Singleton
    Gson gson(){
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    StandingsConfig providesStandingsConfig(){
        return new StandingsConfig();
    }

    @Provides
    @Singleton
    StandingsApi provideStandingsApi(StandingsConfig config, OkHttpClient okHttpClient, Gson gson){
        return new StandingsApi(config, okHttpClient, gson);
    }
}
