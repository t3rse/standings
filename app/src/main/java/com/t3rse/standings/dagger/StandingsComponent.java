package com.t3rse.standings.dagger;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.t3rse.standings.configuration.StandingsConfig;
import com.t3rse.standings.service.StandingsApi;
import com.t3rse.standings.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by DavidSE on 7/8/16.
 */
@Singleton
@Component(modules = {StandingsModule.class})
public interface StandingsComponent {
    StandingsConfig standingsConfig();
    StandingsApi standingsApi();

    OkHttpClient okHttpClient();
    Gson gson();

    void inject(MainActivity mainActivity);
}
