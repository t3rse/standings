package com.t3rse.standings;

import android.app.Application;

import com.t3rse.standings.dagger.DaggerStandingsComponent;
import com.t3rse.standings.dagger.StandingsComponent;
import com.t3rse.standings.dagger.StandingsModule;

import timber.log.Timber;

/**
 * Created by DavidSE on 7/8/16.
 */
public class StandingsApplication extends Application {

    public void startLogging(){
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        Timber.i("Started application");
    }

    public static StandingsComponent component;
    public static StandingsComponent component(){
        return component;
    }
    private void setComponent(StandingsComponent component){
        this.component = component;
    }

    public void createComponents(){
        StandingsComponent component = DaggerStandingsComponent.builder().standingsModule(new StandingsModule()).build();
        setComponent(component);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startLogging();
        createComponents();
    }

}
