package com.t3rse.standings.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.t3rse.standings.R;
import com.t3rse.standings.model.Standing;
import com.t3rse.standings.ui.view.StandingsItemView;

import java.util.List;

/**
 * Created by DavidSE on 7/10/16.
 */
public class StandingsAdapter extends ArrayAdapter<Standing>{
    private final Context adapterContext;
    private List<Standing> standingList;

    public StandingsAdapter(Context adapterContext){
        super(adapterContext, R.layout.standing_item_view);
        this.adapterContext = adapterContext;
    }

    public StandingsAdapter(Context adapterContext, List<Standing> standingList) {
        this(adapterContext);
        this.standingList = standingList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) adapterContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.standing_item_view, parent, false);

        if(standingList != null && standingList.size() > 0){
            ((StandingsItemView)rowView).setModel(standingList.get(position));

        }

        return rowView;
    }

    @Override
    public int getCount() {
        return standingList != null
                ? standingList.size()
                : 0;
    }

    @Override
    public Standing getItem(int position) {
        return super.getItem(position);
    }
}
