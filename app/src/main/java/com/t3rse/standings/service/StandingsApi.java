package com.t3rse.standings.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.t3rse.standings.configuration.StandingsConfig;
import com.t3rse.standings.model.Standing;
import com.t3rse.standings.model.Standings;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import timber.log.Timber;

/**
 * Created by DavidSE on 7/8/16.
 */
public class StandingsApi {

    private final StandingsConfig config;
    private final OkHttpClient httpClient;
    private final Gson gson;

    @Inject
    public StandingsApi(StandingsConfig config, OkHttpClient httpClient, Gson gson) {
        this.config = config;
        this.httpClient = httpClient;
        this.gson = gson;
    }

    public Observable<Standings> getBaseballStandings() {
        return rx.Observable.create(new Observable.OnSubscribe<Standings>(){
            OkHttpClient client = new OkHttpClient();

            @Override
            public void call(Subscriber<? super Standings> subscriber) {
                try{
                    Response okResponse = client.newCall(
                            new Request.Builder().url(config.getEndpoint()).build()
                    ).execute();

                    Standings standingsData = gson.fromJson(okResponse.body().charStream(), Standings.class);
                    if(subscriber != null){
                        subscriber.onNext(standingsData);
                    }
                }
                catch(Exception ex){
                    Timber.e(ex, "Error retrieving");
                }
            }
        });
    }
}
