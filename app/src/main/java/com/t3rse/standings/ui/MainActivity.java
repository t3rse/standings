package com.t3rse.standings.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.t3rse.standings.R;
import com.t3rse.standings.StandingsApplication;
import com.t3rse.standings.adapter.StandingsAdapter;
import com.t3rse.standings.model.Standing;
import com.t3rse.standings.model.Standings;
import com.t3rse.standings.service.StandingsApi;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by DavidSE on 7/8/16.
 */
public class MainActivity extends AppCompatActivity {

    @Bind(R.id.standingsAlWestListView) ListView standingsAlWestListView;
    @Bind(R.id.standingsAlCentralListView) ListView standingsAlCentralListView;
    @Bind(R.id.standingsAlEastListView) ListView standingsAlEastListView;

    @Bind(R.id.standingsNlWestListView) ListView standingsNlWestListView;
    @Bind(R.id.standingsNlCentralListView) ListView standingsNlCentralListView;
    @Bind(R.id.standingsNlEastListView) ListView standingsNlEastListView;

    @Bind(R.id.standingsContainer)
    RelativeLayout standingsContainer;

    StandingsAdapter standingsAdapter;

    @Inject
    StandingsApi standingsApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        ButterKnife.bind(this);

        StandingsApplication.component().inject(this);


        standingsApi.getBaseballStandings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Standings>() {
                    @Override
                    public void onCompleted() {
                        Timber.d("Completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.d("Error", e);
                    }

                    @Override
                    public void onNext(final Standings standings) {
                        MainActivity.this.standingsAlWestListView.setAdapter(
                                new StandingsAdapter(MainActivity.this, standings.getCurrentStandings("AL","W"))
                        );
                        MainActivity.this.standingsAlCentralListView.setAdapter(
                                new StandingsAdapter(MainActivity.this, standings.getCurrentStandings("AL","C"))
                        );
                        MainActivity.this.standingsAlEastListView.setAdapter(
                                new StandingsAdapter(MainActivity.this, standings.getCurrentStandings("AL","E"))
                        );

                        MainActivity.this.standingsNlWestListView.setAdapter(
                                new StandingsAdapter(MainActivity.this, standings.getCurrentStandings("NL","W"))
                        );
                        MainActivity.this.standingsNlCentralListView.setAdapter(
                                new StandingsAdapter(MainActivity.this, standings.getCurrentStandings("NL","C"))
                        );
                        MainActivity.this.standingsNlEastListView.setAdapter(
                                new StandingsAdapter(MainActivity.this, standings.getCurrentStandings("NL","E"))
                        );

                        MainActivity.this.standingsContainer.invalidate();
                    }
                });

    }


}
