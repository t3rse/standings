package com.t3rse.standings.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.t3rse.standings.R;
import com.t3rse.standings.model.Standing;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by DavidSE on 7/10/16.
 */
public class StandingsItemView extends RelativeLayout {

    @Bind(R.id.positionTextView) TextView positionTextView;
    @Bind(R.id.nameTextView) TextView nameTextView;
    @Bind(R.id.recordTextView) TextView recordTextView;
    @Bind(R.id.winningPercentageTextView) TextView winningPercentageTextView;

    Standing model;


    public StandingsItemView(Context context) {
        this(context, null);
    }

    public StandingsItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StandingsItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (isInEditMode()) return;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if(isInEditMode())return;

        ButterKnife.bind(this);

        bind();
    }

    public void setModel(Standing standing){
        this.model = standing;
    }

    public void bind(){
        if(this.model != null){
            positionTextView.setText(String.valueOf(model.getRank()));
            nameTextView.setText(model.getTeamName());
            recordTextView.setText(model.getWinLoss());
            winningPercentageTextView.setText(model.getWin_percentage());
        }
    }
}
