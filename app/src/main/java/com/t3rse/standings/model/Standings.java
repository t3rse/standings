package com.t3rse.standings.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rx.Completable;

/**
 * Created by DavidSE on 7/10/16.
 */
public class Standings {
    String standings_date;

    @SerializedName("standing")
    List<Standing>  currentStandings;

    public List<Standing> getCurrentStandings(){
        return this.currentStandings;
    }

    public List<Standing> getCurrentStandings(String conference, String division){
        ArrayList<Standing> filteredStandings = new ArrayList<Standing>();
        for (Standing standing :
                this.currentStandings) {
            if (standing.conference.equals(conference) && standing.division.equals(division)) {
                filteredStandings.add(standing);
            }
        }

        return filteredStandings;
    }

}
