package com.t3rse.standings.model;

import java.util.ArrayList;

/**
 * Created by DavidSE on 7/8/16.
 */
public class Standing {
    int rank;
    int won;
    int lost;
    String streak;
    String ordinal_rank;
    String first_name;
    String last_name;
    String team_id;
    double games_back;
    int points_for;
    int points_against;
    int home_won;
    int home_lost;
    int away_won;
    int away_lost;
    int conference_won;
    int conference_lost;
    String last_five;
    String last_ten;
    String conference;
    String division;
    int games_played;
    String points_scored_per_game;
    String points_allowed_per_game;
    String win_percentage;
    int point_differential;
    String point_differential_per_game;
    String streak_type;
    int streak_total;

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getWon() {
        return won;
    }

    public void setWon(int won) {
        this.won = won;
    }

    public int getLost() {
        return lost;
    }

    public void setLost(int lost) {
        this.lost = lost;
    }

    public String getStreak() {
        return streak;
    }

    public void setStreak(String streak) {
        this.streak = streak;
    }

    public String getOrdinal_rank() {
        return ordinal_rank;
    }

    public void setOrdinal_rank(String ordinal_rank) {
        this.ordinal_rank = ordinal_rank;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public double getGames_back() {
        return games_back;
    }

    public void setGames_back(double games_back) {
        this.games_back = games_back;
    }

    public int getPoints_for() {
        return points_for;
    }

    public void setPoints_for(int points_for) {
        this.points_for = points_for;
    }

    public int getPoints_against() {
        return points_against;
    }

    public void setPoints_against(int points_against) {
        this.points_against = points_against;
    }

    public int getHome_won() {
        return home_won;
    }

    public void setHome_won(int home_won) {
        this.home_won = home_won;
    }

    public int getHome_lost() {
        return home_lost;
    }

    public void setHome_lost(int home_lost) {
        this.home_lost = home_lost;
    }

    public int getAway_won() {
        return away_won;
    }

    public void setAway_won(int away_won) {
        this.away_won = away_won;
    }

    public int getAway_lost() {
        return away_lost;
    }

    public void setAway_lost(int away_lost) {
        this.away_lost = away_lost;
    }

    public int getConference_won() {
        return conference_won;
    }

    public void setConference_won(int conference_won) {
        this.conference_won = conference_won;
    }

    public int getConference_lost() {
        return conference_lost;
    }

    public void setConference_lost(int conference_lost) {
        this.conference_lost = conference_lost;
    }

    public String getLast_five() {
        return last_five;
    }

    public void setLast_five(String last_five) {
        this.last_five = last_five;
    }

    public String getLast_ten() {
        return last_ten;
    }

    public void setLast_ten(String last_ten) {
        this.last_ten = last_ten;
    }

    public String getConference() {
        return conference;
    }

    public void setConference(String conference) {
        this.conference = conference;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public int getGames_played() {
        return games_played;
    }

    public void setGames_played(int games_played) {
        this.games_played = games_played;
    }

    public String getPoints_scored_per_game() {
        return points_scored_per_game;
    }

    public void setPoints_scored_per_game(String points_scored_per_game) {
        this.points_scored_per_game = points_scored_per_game;
    }

    public String getPoints_allowed_per_game() {
        return points_allowed_per_game;
    }

    public void setPoints_allowed_per_game(String points_allowed_per_game) {
        this.points_allowed_per_game = points_allowed_per_game;
    }

    public String getWin_percentage() {
        return win_percentage;
    }

    public void setWin_percentage(String win_percentage) {
        this.win_percentage = win_percentage;
    }

    public int getPoint_differential() {
        return point_differential;
    }

    public void setPoint_differential(int point_differential) {
        this.point_differential = point_differential;
    }

    public String getPoint_differential_per_game() {
        return point_differential_per_game;
    }

    public void setPoint_differential_per_game(String point_differential_per_game) {
        this.point_differential_per_game = point_differential_per_game;
    }

    public String getStreak_type() {
        return streak_type;
    }

    public void setStreak_type(String streak_type) {
        this.streak_type = streak_type;
    }

    public int getStreak_total() {
        return streak_total;
    }

    public void setStreak_total(int streak_total) {
        this.streak_total = streak_total;
    }


    public String getTeamName(){
        return String.format("%s %s", this.getFirst_name(), this.getLast_name());
    }

    public String getWinLoss(){
        return String.format("%d-%d", this.getWon(), this.getLost());
    }
}
